const mongoose = require("mongoose"),
    Schema = mongoose.Schema;
const fs = require("fs");
const config = require("../Config/Config");
mongoose.connect(
    `mongodb://${config.database.username}:` +
    `${config.database.password}@${config.database.host}:${config.database.port}/${config.database.database}`,
    {useNewUrlParser: true, useUnifiedTopology: true},
    (err) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log("Connecter à la base Mongo")
    });

let Model = [];

Model["guild"] = require("./Guilds.js");

module.exports = Model;