const mongoose = require("mongoose");
const findOrCreate = require('mongoose-findorcreate');

GuildSchema = mongoose.Schema({
    _id: String,
    prefix: String,
    admin: [String]
});
GuildSchema.plugin(findOrCreate);
module.exports = mongoose.model('Guild', GuildSchema);