/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

module.exports = class Morpion extends Commands {
    constructor(client) {
        super(client, {
            name: "morpion",
            description: "Jouez au morpion",
            usage: `morpion`,
            aliases: ["m", "mp"]
        });
    }

    async run(message, args) {
        if (!message.mentions.users.first()) {
            let msg = this.messageEmbed(["Erreur"],
                ["Vous ne pouvez pas lancer une partie seul"],
                "RED");
            await message.reply(msg);
            return;
        }
        if (await this.acceptMatch(message, message.mentions.users.first())) {
            await this.Game(message.author, message.mentions.users.first(), message);
        } else {
            await message.channel.send(`<@${message.author.id}> désolé ton adversaire à refuser`);
        }


    }

    async acceptMatch(message, player) {
        return await message.channel.send(`<@${player.id}> <@${message.author.id}> veux faire un morpion avec toi ?`)
            .then(async msg => {
                await msg.react('👍');
                await msg.react('👎');

                const filter = (reaction, user) => {
                    return ['👍', '👎'].includes(reaction.emoji.name) && user.id === player.id;
                };

                return await msg.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']})
                    .then(collected => {
                        msg.delete();
                        const reaction = collected.first();
                        return reaction.emoji.name === '👍';
                    })
                    .catch(collected => {
                        message.reply('you reacted with neither a thumbs up, nor a thumbs down.');
                    });
            });
    }

    InitTable(table) {
        //1er ligne
        table[0] = "|";
        table[1] = "|";
        table[2] = "|";

        //2eme ligne
        table[3] = "|";
        table[4] = "|";
        table[5] = "|";

        //3eme ligne
        table[6] = "|";
        table[7] = "|";
        table[8] = "|";
        return table
    };

    checkWin(table1, table2, table3, sign) {
        return table1 === sign && table2 === sign && table3 === sign;

    }

    async sendTable(tableIndex, tableValue, message, msg = null) {
        if (msg === null)
            return await message.channel.send(this.messageEmbed(tableIndex, tableValue, "PURPLE", "Morpion", true));
        else
            return await msg.edit(this.messageEmbed(tableIndex, tableValue, "PURPLE", "Morpion", true));

    }

    async casePlayed(value, casePlayed) {
        switch (value) {
            case 0:
                casePlayed.push("0️⃣");
                break;
            case 1:
                casePlayed.push("1️⃣");
                break;
            case 2:
                casePlayed.push("2️⃣");
                break;
            case 3:
                casePlayed.push("3️⃣");
                break;
            case 4:
                casePlayed.push("4️⃣");
                break;
            case 5:
                casePlayed.push("5️⃣");
                break;
            case 6:
                casePlayed.push("6️⃣");
                break;
            case 7:
                casePlayed.push("7️⃣");
                break;
            case 8:
                casePlayed.push("8️⃣");
                break;
        }
        return casePlayed
    }

    getIndex(value) {
        switch (value) {
            case "0️⃣":
                return 0;
            case "1️⃣":
                return 1;
            case "2️⃣":
                return 2;
            case "3️⃣":
                return 3;
            case "4️⃣":
                return 4;
            case "5️⃣":
                return 5;
            case "6️⃣":
                return 6;
            case "7️⃣":
                return 7;
            case "8️⃣":
                return 8;
        }
    }

    IsWin(table, player) {
        if (this.checkWin(table[0], table[1], table[2], player)) return true;
        if (this.checkWin(table[3], table[4], table[5], player)) return true;
        if (this.checkWin(table[6], table[7], table[8], player)) return true;
        if (this.checkWin(table[0], table[3], table[6], player)) return true;
        if (this.checkWin(table[1], table[4], table[7], player)) return true;
        if (this.checkWin(table[2], table[5], table[8], player)) return true;
        if (this.checkWin(table[0], table[4], table[8], player)) return true;
        return this.checkWin(table[2], table[4], table[6], player);

    }

    async turn(TableIndex, TableValue, message, fPlayer, sPlayer, turn, msg = null, playerTurn = null) {
        let turnCount = turn;
        msg = await this.sendTable(TableIndex, TableValue, message, msg);
        let caseUnPlayedAr = [];

        TableValue.forEach(async (value, index) => {
            if (value === "|") {
                await this.casePlayed(index, caseUnPlayedAr)
            }
        });
        
        if (turnCount === 1) {
            for (const value of caseUnPlayedAr) {
                try {
                    await msg.react(value)
                } catch (e) {
                    console.error(e)
                }
            }
        }

        let filter;
        let PlayerSign;
        let PlayerPlay;
        if (turnCount % 2) {
            filter = (reaction, user) => {
                return caseUnPlayedAr.includes(reaction.emoji.name) && user.id === fPlayer.id;
            };
            PlayerSign = "X";
            PlayerPlay = fPlayer;
        } else {
            filter = (reaction, user) => {
                return caseUnPlayedAr.includes(reaction.emoji.name) && user.id === sPlayer.id;
            };
            PlayerSign = "O";
            PlayerPlay = sPlayer;
        }
        if (playerTurn === null) {
            playerTurn = await message.channel.send(`<@${PlayerPlay.id}> c'est à toi de jouer\n Tour: ${turnCount}`);
        } else {
            playerTurn = await playerTurn.edit(`<@${PlayerPlay.id}> c'est à toi de jouer\n Tour: ${turnCount}`)
        }
        await msg.awaitReactions(filter, {max: 1, time: 60000, errors: ['time']}).then(collected => {
            const reaction = collected.first();
            caseUnPlayedAr.forEach(async (value, index) => {
                if (reaction.emoji.name === value) {
                    TableValue[this.getIndex(value)] = PlayerSign;
                    msg.reactions.resolve(reaction).remove()
                }
            })
        });
        if (turnCount === 9) {
            msg.edit(this.messageEmbed(["C'est FINI"],
                [`égalité vous fairez mieux la prochaine fois`],
                "GREY", "Morpion", false));
            return;
        }
        if (this.IsWin(TableValue, PlayerSign)) {
            TableIndex.push("C'est Finie");
            TableValue.push(`Le joueur ${PlayerPlay.username} à gagné`);
            let response = await msg.edit(this.messageEmbed(TableIndex, TableValue, "GREEN", "Morpion", true));
            await new Promise(resolve => setTimeout(resolve, 10000));
            await response.delete();
            await playerTurn.delete();
        } else {
            turnCount = turnCount + 1;
            this.turn(TableIndex, TableValue, message, fPlayer, sPlayer, turnCount, msg, playerTurn);
        }
    }

    async Game(fPlayer, sPlayer, message) {
        let table = await this.InitTable([]),
            turn = 1,
            keys = table.keys(),
            values = table.values(),
            TableIndex = [],
            TableValue = [];
        for (const key of keys) {
            await TableIndex.push(key)
        }
        for (const value of values) {
            await TableValue.push(value)
        }
        this.turn(TableIndex, TableValue, message, fPlayer, sPlayer, turn)


    }


};
