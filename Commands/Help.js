/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

module.exports = class Help extends Commands {
    constructor(client) {
        super(client, {
            name: "help",
            description: "get help for all commande",
            usage: `help (commands)`,
            aliases: ["h", "?"]
        });
    }

    async run(message, args) {
        let name = [];
        let value = [];
        console.log(this.client.config)
        await this.client.commands.forEach(cmd => {
            name.push(cmd.help.name);
            value.push(`${cmd.help.description}\nUtilisation: \`${this.client.config.prefix}${cmd.help.usage}\``)
        });
        await message.author.send(super.messageEmbed(name, value, "ba3b3b",
            "Aide", true))

    }
};
