/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

module.exports = class Pileouface extends Commands {
    constructor(client) {
        super(client, {
            name: "pof",
            description: "Joue à Pile ou face",
            usage: `pof`,
            aliases: []
        });
    }

    async run(message, args) {
        if (!args[0]) {
            let msg = this.messageEmbed(["**Votre partie contre moi**"],
                ["Choisissez votre signe"],
                "ORANGE", "Shifumi");
            await message.reply(msg).then(async (msg) => {
                const choice = ['🌕', '🌑'];
                await msg.react(choice[0]);
                await msg.react(choice[1]);
                const filter = (reaction, user) => {
                    return choice.includes(reaction.emoji.name) && user.id === message.author.id
                };
                let botChoice = await choice[Math.floor(Math.random() * choice.length)];
                msg.awaitReactions(filter, {max: 1, time: 60000, errors: ["time"]}).then(
                    async collected => {
                        await msg.delete();
                        const reaction = collected.first();
                        let result
                        if (reaction.emoji.name === botChoice) {
                            result = await this.messageEmbed(["le pièce est tombé sur", "Bravo"],
                                [botChoice, "Tu as Gagné"],
                                "GREEN", "Pile ou face", false);
                        } else {
                            result = await this.messageEmbed(["le pièce est tombé sur", "Dommage"],
                                [botChoice, "Tu as Perdu"],
                                "RED", "Pile ou face", false);
                        }
                        let piece = "https://cdn.discordapp.com/attachments/736585337459179560/737940808224866406/piece.gif?size=64"
                        result.setImage(piece)
                        let response = await message.channel.send(result);
                        await new Promise(resolve => setTimeout(resolve, 5000));
                        await response.delete();
                    }
                )
            })
        }
    }
};
