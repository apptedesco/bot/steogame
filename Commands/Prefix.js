/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');
const Guild = require("../Models/DB")['guild'];
module.exports = class Morpion extends Commands {
    constructor(client) {
        super(client, {
            name: "prefix",
            description: "Changer de prefix",
            usage: `prefix`,
            aliases: ["p"]
        });
    }

    async run(message, args) {
        if (!message.member.hasPermission('ADMINISTRATOR')) {
            await message.author.send("Désolé tu n'as pas la permission")
            return
        }
        if (args[0] === undefined || args[0] === "") {
            await message.author.send("Tu dois ajouter un prefix en argument")
            return
        }

        await Guild.findOneAndUpdate({_id: message.guild.id}, {prefix: args[0]})
        let newguild = await Guild.findOne({_id: message.guild.id})
        console.log(newguild)

    }

};
