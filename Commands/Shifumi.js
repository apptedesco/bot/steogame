/**
 * @author: steodec
 * @copyright: Steodec
 */
const Commands = require('../Base/Commands.js');

module.exports = class Shifumi extends Commands {
    constructor(client) {
        super(client, {
            name: "shifumi",
            description: "Joue à pierre papier ciseaux",
            usage: `shifumi {joueur}`,
            aliases: ["s", "pps"]
        });
    }

    async run(message, args) {
        if (!args[0]) {
            let msg = this.messageEmbed(["**Votre partie contre moi**"],
                ["Choisissez votre signe"],
                "ORANGE", "Shifumi");
            await message.reply(msg).then(async (msg) => {
                const choice = ['✂', '📄', '🧱'];
                await msg.react(choice[0]);
                await msg.react(choice[1]);
                await msg.react(choice[2]);
                const filter = (reaction, user) => {
                    return choice.includes(reaction.emoji.name) && user.id === message.author.id
                };
                let botChoice = await choice[Math.floor(Math.random() * choice.length)];
                msg.awaitReactions(filter, {max: 1, time: 60000, errors: ["time"]}).then(
                    async collected => {
                        await msg.delete();
                        const reaction = collected.first();
                        let result
                        if (reaction.emoji.name === botChoice) {
                            result = await this.messageEmbed(["le bot à choisi", "égalité"],
                                [botChoice, "Dommage un prochaine fois"],
                                "GREY", "Dommage", false);
                        }
                        if (reaction.emoji.name === choice[0] && botChoice === choice[1]) {
                            result = await this.messageEmbed(["le bot à choisi", "Gagné"],
                                [botChoice, "bravo tu as gagné"],
                                "GREEN", "Gagné", false);
                        }
                        if (reaction.emoji.name === choice[1] && botChoice === choice[2]) {
                            result = await this.messageEmbed(["le bot à choisi", "Gagné"],
                                [botChoice, "bravo tu as gagné"],
                                "GREEN", "Gagné", false);
                        }
                        if (reaction.emoji.name === choice[2] && botChoice === choice[0]) {
                            result = await this.messageEmbed(["le bot à choisi", "Gagné"],
                                [botChoice, "bravo tu as gagné"],
                                "GREEN", "Gagné", false);
                        }
                        if (reaction.emoji.name === choice[1] && botChoice === choice[0]) {
                            result = await this.messageEmbed(["le bot à choisi", "Perdu"],
                                [botChoice, "Dommage tu as perdu"],
                                "RED", "Dommage", false);
                        }
                        if (reaction.emoji.name === choice[2] && botChoice === choice[1]) {
                            result = await this.messageEmbed(["le bot à choisi", "Perdu"],
                                [botChoice, "Dommage tu as perdu"],
                                "RED", "Dommage", false);
                        }
                        if (reaction.emoji.name === choice[0] && botChoice === choice[2]) {
                            result = await this.messageEmbed(["le bot à choisi", "Perdu"],
                                [botChoice, "Dommage tu as perdu"],
                                "RED", "Dommage", false);
                        }
                        let response = await message.channel.send(result);
                        await new Promise(resolve => setTimeout(resolve, 5000));
                        await response.delete();
                    }
                )
            })
        }
    }
};
