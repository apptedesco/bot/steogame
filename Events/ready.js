const Guild = require("../Models/DB")["guild"];
module.exports = class {
    constructor(client) {
        this.client = client;
    }

    run() {
        console.info(`SteoGame: Logged is as ${this.client.user.tag}`);
        Guild.countDocuments({}, (err, result) => {
            this.client
                .user.setActivity(`SteoGame sur ${result} guild(s)`, {type: "PLAYING"});
        })
    };
};
