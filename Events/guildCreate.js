const Guild = require("../Models/DB")["guild"];

module.exports = class {
    constructor(client) {
        this.client = client;
    }

    run(guild) {
        console.log(`New guild added : ${guild.name}, owned by ${guild.owner.user.username}`);
        Guild.findOrCreate({_id: guild.id, prefix: "*", admin: [""]}, (err) => {
            if (err) throw err;
        })
    }
};
